/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** BOMB
*/

#include "Bomb.hpp"
#include <ctime>
#include "../Player/Player.hpp"
#include "../Map/Map.hpp"
#include "../Factory/Factory.hpp"

using namespace std::chrono_literals;

Bomb::Bomb(ISceneManager &scene, IVideoDriver &driver, const vector3df &pos, Map *map, int range, std::string preprend)
{
    std::srand(unsigned(std::time(0)));
    _posX = static_cast<int>(std::round(pos.X / MAP_SPACING));
    _posY = static_cast<int>(std::round(pos.Y / MAP_SPACING));
    const vector3df new_pos((float)this->_posX * MAP_SPACING, (float)this->_posY * MAP_SPACING, (float)pos.Z);
	this->_range = range;
	_scene = &scene;
	_driver = &driver;
	_map = map;
    _prepend = preprend;
	this->name = "Bomb";
	this->status = true;
	this->traversibility = true;
	CreateIrrlichtObject(scene, driver, new_pos);
}

std::string Bomb::getName() const
{
	return(this->name);
}

void Bomb::setName(std::string name)
{
    this->name = name;
}

void Bomb::delay(Player *one)
{
    std::mutex mu;
    mu.lock();
    countdown();
    one->addBomb();
    mu.unlock();
}

vector_t Bomb::getPosition() const
{
	return(vector_t{_posX, _posY});
}

bool Bomb::getStatus() const
{
	return(this->status);
}

void Bomb::destroyIrrlicht()
{
    _scene->addToDeletionQueue(node);
}

void Bomb::changeBlocsName()
{
    auto mapObject = _map->getMapObject();
    deleteBlock(_posX, _posY, mapObject);
    bombUp(mapObject);
    bombDown(mapObject);
    bombLeft(mapObject);
    bombRight(mapObject);
}


void Bomb::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const  vector3df &position)
{
    _mesh = scene.getMesh((_prepend + UP_PATH + "assets/tntbarrel/tntbarrel.MD3").c_str());
    node = scene.addAnimatedMeshSceneNode(_mesh, 0, -1, position, vector3df(90, 180, 0), vector3df((float)0.55, (float)0.55, (float)0.55));
    if (this->node) {
        this->node->setMaterialFlag(EMF_LIGHTING, false);
        node->setMaterialTexture(0, driver.getTexture((_prepend + UP_PATH + "assets/tntbarrel/TNTbarrel.bmp").c_str()));
    }
}

void Bomb::SetMeshPosition(const vector3df &pos)
{
    this->node->setPosition(pos);
}

void Bomb::bombDown(object_map map_object)
{
    for (int offset = 1; offset <= _range; offset++) {
        if (!deleteBlock(_posX, _posY - offset, map_object))
            break;
    }
}

void Bomb::bombUp(object_map map_object)
{
    for (int offset = 1; offset <= _range; offset++) {
        if (!deleteBlock(_posX, _posY + offset, map_object))
            break;
    }
}

void Bomb::bombLeft(object_map map_object)
{
    for (int offset = 1; offset <= _range; offset++) {
        if (!deleteBlock(_posX - offset, _posY, map_object))
            break;
    }
}

void Bomb::bombRight(object_map map_object)
{
    for (int offset = 1; offset <= _range; offset++) {
        if (!deleteBlock(_posX + offset, _posY, map_object))
            break;
    }
}

void Bomb::playerKiller(std::shared_ptr<Player> player, int x, int y)
{
    auto pos = player->getAbsolutePosition();
    if (pos.x == x && pos.y == y) {
        player->die();
    }
}

void Bomb::generateVFX(int x, int y)
{
    IParticleSystemSceneNode* ps =
            _scene->addParticleSystemSceneNode(false);
    IParticleEmitter* em = ps->createBoxEmitter(
            core::aabbox3d<f32>(-7,0,-7,7,1,7),
            core::vector3df(0.0f,0.06f,0.0f),
            80,100,
            video::SColor(0,255,255,255),
            video::SColor(0,255,255,255),
            800,1600,0,
            core::dimension2df(100.f,100.f),
            core::dimension2df(15.f,15.f));
    ps->setEmitter(em);
    em->drop();
    IParticleAffector* paf = ps->createFadeOutParticleAffector();
    ps->addAffector(paf);
    paf->drop();
    ps->setPosition(core::vector3df((float)65 * x, (float)65 * y, (float)20));
    ps->setScale(core::vector3df(2,2,2));
    ps->setMaterialFlag(EMF_LIGHTING, false);
    ps->setMaterialFlag(EMF_ZWRITE_ENABLE, false);
    ps->setMaterialType(EMT_TRANSPARENT_ADD_COLOR);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    ps->setEmitter(0);
}

bool Bomb::deleteBlock(int x , int y, object_map map_object)
{
    if (x <= 0 || y <= 0 || x >= 24 || y >= 23)
        return false;
    auto players = _map->getPlayersVector();
    int rand = std::rand() % 100;
    auto object = map_object[x][y].first;
    auto objName = object->getName();
    if (objName == "Undestructable")
        return false;
    else {
        generateVFX(x, y);
        std::for_each(players.begin(), players.end(), [this, x, y] (std::shared_ptr<Player> player) {playerKiller(player, x, y);});
        if (objName == "Traversable")
            return true;
        if (objName == "Destructable") {



        object->destroy(_map);
        if (rand < POWERUP_SPAWN_RATE)
            spawnBonus(x, y);
        return true;
    }
    }
//    unreachable;
    return false;

}

void Bomb::spawnBonus(int x, int y)
{
    int rand = std::rand() % 4;

    switch (rand) {
        case 0:
            _map->generatePowerUp(BombUpPower, x, y, *_scene, *_driver);
            break;
        case 1:
            _map->generatePowerUp(RangeUpPower, x, y, *_scene, *_driver);
            break;
        case 2:
            _map->generatePowerUp(SpeedUpPower, x, y, *_scene, *_driver);
            break;
        case 3:
            _map->generatePowerUp(WallpassPower, x, y, *_scene, *_driver);
            break;
    }
}

bool Bomb::countdown()
{
    std::this_thread::sleep_for(std::chrono::seconds(EXPLOSION_DELAY));
    destroyIrrlicht();
    changeBlocsName();
    return true;
}
