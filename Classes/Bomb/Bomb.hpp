/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** BOMB
*/

#ifndef BOMB_HPP_
#define BOMB_HPP_

#include <vector>
#include <cstdio>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>
#include <cmath>
#include <future>
#include <cstdlib>
#include "../DestructableBlock/DestructableBlock.hpp"
#include "../TraversableBlock/TraversableBlock.hpp"
#define EXPLOSION_DELAY 2
#define POWERUP_SPAWN_RATE 15

typedef std::vector<std::vector<std::pair<std::shared_ptr<IObject>, vector_t>>> object_map;

class Player;
class Map;

class Bomb : public IObject {
	public:
		Bomb(ISceneManager &, IVideoDriver &, const  vector3df &, Map *map, int range = 2,  std::string preprend = "");
        std::string getName() const override;
        vector_t getPosition() const override;
        bool getStatus() const override;
        void setTriangleSelector(ITriangleSelector *new_selector) override {};
        void setName(std::string name) override;

    void SetMeshPosition(const vector3df &pos) override;
        void CreateIrrlichtObject(ISceneManager &, IVideoDriver &, const  vector3df &) override;
        void delay(Player *one);
        void destroy(Map *) override {};
        void spawnBonus(int x, int y);
        void generateVFX(int, int);

        void destroyIrrlicht();
        bool countdown();
        void changeBlocsName();
        bool deleteBlock(int, int, object_map);
        void bombUp(object_map);
        void bombLeft(object_map);
        void bombRight(object_map);
        void bombDown(object_map);
        void playerKiller(std::shared_ptr<Player>, int, int);
        IAnimatedMeshSceneNode *getModel() override { return node;};
	private:
		int _posX;
		int _posY;
		int _range;
        std::string name;
        bool status;
        bool traversibility;
        IAnimatedMeshSceneNode *node;
        IAnimatedMesh *_mesh;
        ISceneManager* _scene;
        Map *_map;
        std::string _prepend;
        IVideoDriver* _driver;
};

#endif /* !BOMB_HPP_ */
