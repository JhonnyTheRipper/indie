/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_four
*/

#ifndef INDIE_PLAYER_FOUR_HPP
#define INDIE_PLAYER_FOUR_HPP

#include "../Player/Player.hpp"

class Player_four: public Player {
public:
    Player_four(MastEventReceiver *, Map *map);
    void bombEvent() override ;
    Direction movementEvent() override ;
    void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override;
};


#endif //INDIE_PLAYER_FOUR_HPP
