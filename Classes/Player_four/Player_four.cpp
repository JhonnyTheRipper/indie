/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_four
*/

#include "Player_four.hpp"
#include "../Classes/Game/Game.hpp"


Player_four::Player_four(MastEventReceiver *receiver, Map *map)
{
    ghosty = false;
    movespeedMultiplier = 0;
    range = 2;
    _map = map;
    _receiver = receiver;
    name = "First";
    traversibility = true;
    status = true;
    active_bombs = 1;
    _keyUp = KEY_UP;
    _keyDown = KEY_DOWN;
    _keyLeft = KEY_LEFT;
    _keyRight = KEY_RIGHT;
    _keyBomb = KEY_KEY_M;
}

void Player_four::bombEvent()
{
    handleEvent();
}

Direction Player_four::movementEvent()
{
    return movement();
}


void Player_four::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const vector3df &position)
{
    _scene = &scene;
    _driver = &driver;
    _position = position;
    loadModel(Draedlord);
}