/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Map
*/

#include "Map.hpp"
#include <iostream>
#include "../Classes/Game/Game.hpp"


Map::Map(MastEventReceiver *receiver, std::string prepend): _receiver(receiver), _prepend(prepend)
{
    std::string line;
    char str[25];
    this->pos.x = 0;
    this->pos.x = 0;
    std::vector<std::string> lines;
    std::string tt = (_prepend + UP_PATH + "Classes/Map/Map.txt");
	std::ifstream in(tt.c_str());
    if (!in) {
        std::cout<<"cannot open Map.txt"<<std::endl;
        exit(84);
    }
    while (in) {
        in.getline(str, 25);
        this->map.push_back(std::string(str));
    }
    this->putDestructableBlock();
  in.close();
}

void Map::putDestructableBlock()
{
    int rand_nb = 0;
    srand((unsigned)time(0));
    for (int i = 2; i < 23; i += 1) {
        for (int j = 1; j < 23; j += 2) {
            rand_nb = (rand()%2);
            if (rand_nb == 1)
                this->map[i][j] = 'D';
        } 
    }
}

object_map Map::getMapObject()
{
    return (this->mapObject);
}

void Map::createMapObject()
{
    base::Factory factory;
    mapObject.resize(25);
    for (int x = 0; x < 25; x++) {
        this->pos.x = x;
        for (int y = 0; y < 25; y++) {
            this->pos.y = y;
            switch (this->map[x][y]) {
                case 'X':
                    this->mapObject[x].push_back(std::make_pair(factory.createObject("ud_block", _receiver), pos));
                    break;
                case '1':
                    player_one = factory.createObject("playerOne", _receiver, this);
                    _players.push_back(dynamic_pointer_cast<Player>(player_one));
                    this->mapObject[x].push_back(std::make_pair(player_one, pos));
                    break;
                case '2':
                    player_two = factory.createObject("playerTwo", _receiver, this);
                    _players.push_back(dynamic_pointer_cast<Player>(player_two));
                    this->mapObject[x].push_back(std::make_pair(player_two, pos));
                    break;
                case '3':
                    player_three = factory.createObject("playerThree", _receiver, this);
                    _players.push_back(dynamic_pointer_cast<Player>(player_three));
                    this->mapObject[x].push_back(std::make_pair(player_three, pos));
                    break;
                case '4':
                    player_four = factory.createObject("playerFour", _receiver, this);
                    _players.push_back(dynamic_pointer_cast<Player>(player_four));
                    this->mapObject[x].push_back(std::make_pair(player_four, pos));
                    break;
                case 'O':
                    this->mapObject[x].push_back(std::make_pair(factory.createObject("tr_block", _receiver), pos));
                    break;
                case 'D':
                    this->mapObject[x].push_back(std::make_pair(factory.createObject("dr_block", _receiver), pos));
                    break;
            default:
                break;
            }
        }
    }
}

void Map::render(ISceneManager &scene, IVideoDriver &driver)
{
    ITriangleSelector *selector;
    for (auto line :mapObject) {
        for (auto obj : line)
            obj.first->CreateIrrlichtObject(scene, driver, vector3df((float)MAP_SPACING * obj.second.x, (float)MAP_SPACING * obj.second.y, (float)0.0));
    }
    _meta_selector = scene.createMetaTriangleSelector();
    for (auto line: mapObject) {
        for (auto obj : line) {
            if (!obj.first->getName().compare("First"))
                continue;
            selector = scene.createTriangleSelectorFromBoundingBox(obj.first->getModel());
            if (selector) {
                _meta_selector->addTriangleSelector(selector);
                obj.first->setTriangleSelector(selector);
                selector->drop();
            }
        }
    }
}

void Map::setCollision(ISceneManager &scene)
{

    for (size_t i = 0; i < _players.size(); i++)
    {
        ISceneNodeAnimator *anim = scene.createCollisionResponseAnimator(_meta_selector,
                                                                     _players[i]->getModel(), irr::core::vector3df(4, 4, 4),
                                                                     irr::core::vector3df(0.f, 0.f, 0.f),
                                                                     irr::core::vector3df(0.f, 0.f, 0.f));
    _players[i]->getModel()->addAnimator(anim);
    anim->drop();
    }
    
}

void Map::removeSelector(ITriangleSelector *selector)
{
    _meta_selector->removeTriangleSelector(selector);
}

void Map::handlePowerPickUp(std::shared_ptr<Player> player)
{
    base::Factory _new_fac;
    vector_t position = player->getAbsolutePosition();
    auto name = mapObject[position.x][position.y].first->getName();
    if (name == "BombUp") {
        player->pickUpBombUp();
        mapObject[position.x][position.y].first = _new_fac.createObject("tr_block", nullptr);
        return;
    } else if (name == "FireUp") {
        player->pickUpRangeUp();
        mapObject[position.x][position.y].first = _new_fac.createObject("tr_block", nullptr);
        return;
    } else if (name == "SpeedUp") {
        player->pickUpSpeedUp();
        mapObject[position.x][position.y].first = _new_fac.createObject("tr_block", nullptr);
        return;
    } else if (name == "WallPass") {
        player->pickUpWallpass();
        mapObject[position.x][position.y].first = _new_fac.createObject("tr_block", nullptr);
        return;
    }
}

void Map::playerEvent(std::shared_ptr<Player> player)
{
    Direction dir = player->movementEvent();
    switch (dir) {
        case Up:
            player->moveUp();
            break;
        case Down:
            player->moveDown();
            break;
        case Left:
            player->moveLeft();
            break;
        case Right:
            player->moveRight();
            break;
        case AnimationStop:
            player->stopAnimation();
            break;
        default:
            break;
    }
    player->bombEvent();
    handlePowerPickUp(player);
}

bool Map::playersRunEvents()
{
    int a = 0;
    for (auto player : _players) {
        if (!player->getStatus())
            a = a + 1;
    }
    if (a >= 3)
        return true;
    std::for_each(_players.begin(), _players.end(), [this] (std::shared_ptr<Player> player) {playerEvent(player);});
    return false;
}

void Map::generatePowerUp(PowerUp number, int x, int y,
                          ISceneManager &scene, IVideoDriver &driver)
{
    base::Factory _new_fac;
    if (number == BombUpPower) {
        auto power = _new_fac.createObject("bomb_up", nullptr);
        mapObject[x][y].first = power;
        mapObject[x][y].first->CreateIrrlichtObject(scene, driver, vector3df((float)x * MAP_SPACING, (float)y * MAP_SPACING, 0.0));
    } else if (number == RangeUpPower) {
        auto power = _new_fac.createObject("range_up", nullptr);
        mapObject[x][y].first = power;
        mapObject[x][y].first->CreateIrrlichtObject(scene, driver, vector3df((float)x * MAP_SPACING, (float)y * MAP_SPACING, 0.0));
    } else if (number == SpeedUpPower) {
        auto power = _new_fac.createObject("speed_up", nullptr);
        mapObject[x][y].first = power;
        mapObject[x][y].first->CreateIrrlichtObject(scene, driver, vector3df((float)x * MAP_SPACING, (float)y * MAP_SPACING, 0.0));
    } else {
        auto power = _new_fac.createObject("wallpass", nullptr);
        mapObject[x][y].first = power;
        mapObject[x][y].first->CreateIrrlichtObject(scene, driver, vector3df((float)x * MAP_SPACING, (float)y * MAP_SPACING, 0.0));
    }
}