/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Map
*/

#ifndef MAP_H_
#define MAP_H_

#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <memory>
#include "../IObject/IObject.hpp"
#include "../Factory/Factory.hpp"
#include "../../include/header.hpp"
#include "../EventReicever/EventReicever.hpp"
#include "../powerUP/bombUP.hpp"
#include "../powerUP/FireAll.hpp"
#include "../powerUP/speedUP.hpp"
#include "../powerUP/Wallpass.hpp"
#include "../Mast.hpp"

enum PowerUp{
    BombUpPower,
    RangeUpPower,
    SpeedUpPower,
    WallpassPower
};

typedef std::vector<std::vector<std::pair<std::shared_ptr<IObject>, vector_t>>> object_map;

class Map {
    public:
        Map(MastEventReceiver *, std::string prepend = "");
        void putDestructableBlock();
        void createMapObject();
        void render(ISceneManager &scene, IVideoDriver &driver);
        void setCollision(ISceneManager &);
        bool playersRunEvents();
        void generatePowerUp(PowerUp , int, int,
                             ISceneManager &scene, IVideoDriver &driver);
        void playerEvent(std::shared_ptr<Player>);
        std::vector<std::shared_ptr<Player>> getPlayersVector() {return _players;};
        std::shared_ptr<IObject> get_player_one() { return player_one;};
        std::shared_ptr<IObject> get_player_two() { return player_two;};
        std::shared_ptr<IObject> get_player_three() { return player_three;};
        std::shared_ptr<IObject> get_player_four() { return player_four;};
        void removeSelector(ITriangleSelector *);
        object_map getMapObject();
        IMetaTriangleSelector *getMetaSelector() {return _meta_selector;};
        void handlePowerPickUp(std::shared_ptr<Player> player);
    private:
        std::string _prepend;
        std::vector<std::string> map;
        vector_t pos;
        object_map mapObject;
        MastEventReceiver *_receiver;
        std::vector<std::shared_ptr<Player>> _players;
        std::shared_ptr<IObject> player_one;
        std::shared_ptr<IObject> player_two;
        std::shared_ptr<IObject> player_three;
        std::shared_ptr<IObject> player_four;
        IMetaTriangleSelector *_meta_selector;

};

#endif /* !MAP_H_ */
