//
// Created by bunny on 14/05/19.
//

#include "Player.hpp"
#include "../Map/Map.hpp"

std::string Player::getName() const
{
    return(this->name);
}

void Player::setName(std::string name)
{
}

vector_t Player::getPosition() const
{
    return(this->pos);
}

bool Player::getStatus() const
{
    return(this->status);
}


void Player::SetMeshPosition(const vector3df &pos)
{
    this->node->setPosition(pos);
    _position = pos;
}

void Player::setMeshRotation(const vector3df &vector)
{
    node->setRotation(vector);
}

vector3df Player::getMeshPosition()
{
    return node->getPosition();
}

IAnimatedMeshSceneNode *Player::getModel() {
    if (!status)
        return nullptr;
    return node;
}

void Player::put_bomb()
{
    if (!status)
        return;
    if (active_bombs > 0) {
        Bomb new_bomb(*_scene, *_driver, this->getMeshPosition(), _map, range);
        std::thread threadObject(&Bomb::delay, new_bomb, this);
        threadObject.detach();
        active_bombs--;
    }

}

void Player::stopAnimation(int frame)
{

    if (!status)
        return;
    node->setAnimationSpeed(0);
    node->setCurrentFrame((irr::f32)frame);
}

void Player::beginAnimation()
{
    if (!status)
        return;
    node->setCurrentFrame(0);
    node->setAnimationSpeed(20);
}

void Player::handleEvent()
{
    if (!status)
        return;
    if (_receiver->keyPressed(_keyBomb)) {
        put_bomb();
    }

}

 void Player::die()
 {
    if (status) {
        node->removeAnimators();
        _scene->addToDeletionQueue(node);
        status = !status;
    }
 }

 void Player::pickUpBombUp()
 {
    active_bombs++;
 }

 void Player::pickUpRangeUp()
 {
    range++;
 }

 void Player::pickUpSpeedUp()
 {
     movespeedMultiplier ++;
 }

 void Player::pickUpWallpass()
 {
    if (!status)
        return;
    if (!ghosty) {
        node->removeAnimators();
        ghosty = !ghosty;
    }
 }

Direction Player::iaPlayerThree()
{
    if ((this->play + 1 < time(nullptr))) {
        this->v1 = rand() % 22;
        this->play = (int)time(nullptr);
    }
    else {
        if (v1 == 21)
            put_bomb();
        else if (v1 >= 0 && v1 <= 5)
            return(this->iaMovement(Up));
        else if (v1 > 5 && v1 <= 10)
            return(this->iaMovement(Down));
        else if (v1 > 10 && v1 <= 15)
            return(this->iaMovement(Right));
        else if (v1 > 15 && v1 <= 20)
            return(this->iaMovement(Left));
    }
    return(this->iaMovement(Nothing));
}

Direction Player::iaPlayerTwo()
{
    if ((this->play + 1 < time(nullptr))) {
        this->v1 = rand() % 22;
        this->play = (int)time(nullptr);
    }
    else {
        if (v1 == 21)
            put_bomb();
        else if (v1 >= 0 && v1 <= 5)
            return(this->iaMovement(Up));
        else if (v1 > 5 && v1 <= 10)
            return(this->iaMovement(Down));
        else if (v1 > 10 && v1 <= 15)
            return(this->iaMovement(Right));
        else if (v1 > 15 && v1 <= 20)
            return(this->iaMovement(Left));
    }
    return(this->iaMovement(Nothing));
}

 vector_t Player::getAbsolutePosition()
 {
    if (!status)
        return vector_t{0, 0};
     vector3df pos = node->getPosition();
     return (vector_t{static_cast<int>(std::round(pos.X / MAP_SPACING)), static_cast<int>(std::round(pos.Y / MAP_SPACING))});
 }

Direction Player::iaMovement(int move)
 {
     if (move == Down) {
         return Down;
     }
     else if(move == Left) {
         return Left;
     }
     else if (move == Up) {
         return Up;
     }
     else if(move == Right) {
         return Right;
     }
     else {
         return AnimationStop;
     }

//     unreachable part
    return Nothing;
 }

Direction Player::movement()
 {
     if (_receiver->keyDown(_keyDown)) {
         return Down;
     }
     else if(_receiver->keyDown(_keyLeft)) {
         return Left;
     }
     else if (_receiver->keyDown(_keyUp)) {
         return Up;
     }
     else if(_receiver->keyDown(_keyRight)) {
         return Right;
     }
     if (_receiver->keyUp(_keyLeft) && _receiver->keyUp(_keyRight) && _receiver->keyUp(_keyUp) && _receiver->keyUp(_keyDown)) {
         return AnimationStop;
     }

//     unreachable part
    return Nothing;
 }

// Model loading part

void Player::loadModel(Model id)
{
    std::string pathToModel_back;
    std::string pathToTexture_back;
    switch (id) {
        case Archmage:
            pathToModel_back = (_prepend + UP_PATH + "assets/archmage/archmage.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/archmage/HeroArchmage.bmp");
            break;
        case BloodElf:
            pathToModel_back = (_prepend + UP_PATH + "assets/blood_elf/blood_elf.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/blood_elf/Hero_BloodElf.bmp");
            break;
        case Draedlord:
            pathToModel_back = (_prepend + UP_PATH + "assets/dread_lord/dread_lord.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/dread_lord/HeroDreadlord.bmp");
            break;
        case KilJaedan:
            pathToModel_back = (_prepend + UP_PATH + "assets/kil/kil_jaedan.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/kil/Kiljaeden.bmp");
            break;
        case Furion:
            pathToModel_back = (_prepend + UP_PATH + "assets/Furion/Furion_v1.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/Furion/Furion.bmp");
            break;
        case Lich:
            pathToModel_back = (_prepend + UP_PATH + "assets/kel_thuzed/hero_v2.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/kel_thuzed/HeroLich-0.bmp");
            break;
        case Priest:
            pathToModel_back = (_prepend + UP_PATH + "assets/Priestress/prist_v1.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/Priestress/priest.bmp");
            break;
        case Sylvana:
            pathToModel_back = (_prepend + UP_PATH + "assets/sylvanas/sylvana.MD3");
            pathToTexture_back = (_prepend + UP_PATH + "assets/sylvanas/BansheeRanger.bmp");
            break;
        default:
            return;
    }
    const char *pathToModel = pathToModel_back.c_str();
    const char *pathToTexture = pathToTexture_back.c_str();

    ITriangleSelector *selector = 0;
    _mesh = _scene->getMesh(pathToModel);
    node = _scene->addAnimatedMeshSceneNode(_mesh, 0, -1, _position, vector3df(90, 180 , 0), PLAYER_SCALE);
    if (node) {
        node->setMaterialFlag(EMF_LIGHTING, false);
        node->setMaterialTexture(0, _driver->getTexture(pathToTexture));
        stopAnimation(3);
        selector = _scene->createOctreeTriangleSelector(node->getMesh(), node, 64);
        node->setTriangleSelector(selector);
#ifdef NDEBUG
        node->setDebugDataVisible((E_DEBUG_SCENE_TYPE)(node->isDebugDataVisible()^EDS_BBOX));
        node->setDebugDataVisible((E_DEBUG_SCENE_TYPE)(node->isDebugDataVisible()^EDS_BBOX_BUFFERS));
#endif
    }
}

// Movement part

void Player::moveUp()
{
    if (!status)
        return;
    auto pos = node->getPosition();
    auto abs = getAbsolutePosition();
    if (abs.y == 23)
        return;
    if (pos.X - (int)pos.X != 0 || pos.Y - (int)pos.Y != 0) {
        SetMeshPosition(vector3df((float)std::round(pos.X), (float)std::round(pos.Y - 1), (float)std::round(pos.Z)));
    }
    if (!animated) {
        animated = !animated;
        beginAnimation();
    }
    SetMeshPosition(vector3df( _position.X, _position.Y + MOVEMENT_SPEED + movespeedMultiplier, _position.Z));
    setMeshRotation(vector3df(90, 180, 270));
}

void Player::moveDown()
{
    if (!status)
        return;
    auto pos = node->getPosition();
    auto abs = getAbsolutePosition();
    if ( abs.y == 0)
        return;
    if (pos.X - (int)pos.X != 0 || pos.Y - (int)pos.Y != 0) {
        SetMeshPosition(vector3df((float)std::round(pos.X), (float)std::round(pos.Y + 1), (float)std::round(pos.Z)));
    }
    if (!animated) {
        animated = !animated;
        beginAnimation();
    }
    SetMeshPosition(vector3df( _position.X, _position.Y - MOVEMENT_SPEED - movespeedMultiplier, _position.Z));
    setMeshRotation(vector3df(90, 180, 90));
}

void Player::moveLeft()
{
    if (!status)
        return;
    auto pos = node->getPosition();
    auto abs = getAbsolutePosition();
    if (abs.x == 0)
        return;
    if (pos.X - (int)pos.X != 0 || pos.Y - (int)pos.Y != 0) {
        SetMeshPosition(vector3df((float)std::round(pos.X + 1), (float)std::round(pos.Y), (float)std::round(pos.Z)));
    }
    if (!animated) {
        animated = !animated;
        beginAnimation();
    }
    SetMeshPosition(vector3df( _position.X - MOVEMENT_SPEED - movespeedMultiplier, _position.Y, _position.Z));
    setMeshRotation(vector3df(90, 180, 0));
}

void Player::moveRight()
{
    if (!status)
        return;
    auto pos = node->getPosition();
    auto abs = getAbsolutePosition();
    if (abs.x == 24)
        return;
    if (pos.X - (int)pos.X != 0 || pos.Y - (int)pos.Y != 0) {
        SetMeshPosition(vector3df((float)std::round(pos.X - 1), (float)std::round(pos.Y), (float)std::round(pos.Z)));
    }
    if (!animated) {
        animated = !animated;
        beginAnimation();
    }
    SetMeshPosition(vector3df( _position.X + MOVEMENT_SPEED + movespeedMultiplier, _position.Y, _position.Z));
    setMeshRotation(vector3df(90, 180, 180));
}

void Player::stopAnimation() {
    animated = false;
    stopAnimation(3);
}