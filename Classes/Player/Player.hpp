//
// Created by bunny on 14/05/19.
//

#ifndef INDIE_IPLAYER_H
#define INDIE_IPLAYER_H

#include "../IObject/IObject.hpp"
#include "../Bomb/Bomb.hpp"
#include "../EventReicever/EventReicever.hpp"
#include "../Mast.hpp"
#include <chrono>
#include <thread>
#define PLAYER_SCALE vector3df(0.5, 0.5, 0.5)
#define PLAYER_Z_OFFSET 2

typedef std::vector<std::vector<std::pair<std::shared_ptr<IObject>, vector_t>>> object_map;
enum Model {
    Archmage,
    BloodElf,
    Draedlord,
    KilJaedan,
    Furion,
    Lich,
    Priest,
    Sylvana
};

enum Direction {
    Up,
    Down,
    Left,
    Right,
    AnimationStop,
    Nothing
};

class Map;

class Player : virtual public IObject {
    public:
        std::string getName() const override;
        vector_t getPosition() const override;
        bool getStatus() const override;
        void setName(std::string name) override;

    void SetMeshPosition(const vector3df &) override;
        void setMeshRotation(const vector3df &);
        IAnimatedMeshSceneNode *getModel() override;
        virtual void bombEvent() = 0;
        virtual Direction movementEvent() = 0;
        vector3df getMeshPosition();
        vector_t getAbsolutePosition();
        void setTriangleSelector(ITriangleSelector *new_selector) override {};

        void handleEvent();
        Direction movement();
        void put_bomb();
        void stopAnimation();

        void moveUp();
        void moveDown();
        void moveLeft();
        void moveRight();

        void pickUpBombUp();
        void pickUpRangeUp();
        void pickUpSpeedUp();
        void pickUpWallpass();

        void destroy(Map *) override {};
        void die();

        void stopAnimation(int frame);
        void beginAnimation();

        int getActiveBombs() const {return active_bombs;};
        std::vector<Bomb> getBombVector() const {return _plantedBombs;};
        void addBomb() {active_bombs++;};
        void loadModel(Model);
        Direction iaPlayerThree();
        Direction iaPlayerTwo();
        Direction iaMovement(int move);

    protected:
        typedef std::pair<irr::scene::IAnimatedMeshSceneNode *, irr::scene::ISceneNodeAnimator *> CollisionInfo;
        std::string name;
        vector_t pos;
        bool status;
        bool traversibility;
        int active_bombs;
        int range;
        IAnimatedMeshSceneNode *node;
        vector_t _Position;
        vector_t iaActualPos;
        IAnimatedMesh *_mesh;
        ISceneManager* _scene;
        IVideoDriver* _driver;
        ITriangleSelector *_selector;
        vector3df _position;
        MastEventReceiver *_receiver;
        Map *_map;
        int movespeedMultiplier;
        bool ghosty;
        int new_y;
        std::string _prepend;   
        std::vector<Bomb> _plantedBombs;
        std::vector<CollisionInfo>	_collisionTab;
        const float MOVEMENT_SPEED = 5;
        bool animated;
        char _keyUp;
        char _keyDown;
        char _keyLeft;
        char _keyRight;
        char _keyBomb;
        int play = 0;
        int v1 = 0;
};


#endif //INDIE_IPLAYER_H
