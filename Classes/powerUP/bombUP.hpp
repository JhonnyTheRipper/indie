/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** bomb_up
*/

#ifndef BOMBUP_HPP_
#define BOMBUP_HPP_

#include <vector>
#include <cstdio>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include "../IObject/IObject.hpp"

class Map;

class BombUp : public IObject{
	public:
		BombUp();
		~BombUp() override {
            if (status) {
                _scene->addToDeletionQueue(node);
                status = !status;
            }
		}
		void setName(std::string name) override;
		std::string getName() const override ;
		vector_t getPosition() const override ;
		bool getStatus() const override ;

    IAnimatedMeshSceneNode *getModel() override { return node;};
		void SetMeshPosition(const vector3df &pos) override;
		void destroy(Map *) override {
		};
        void setTriangleSelector(ITriangleSelector *new_selector) override {};
		void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const  vector3df &) override ;
	private:
		std::string name;
		vector_t pos;
		bool status;
		bool traversibility;
		int nbBomb;
        IAnimatedMeshSceneNode *node;
        IAnimatedMesh *_mesh;
		ISceneManager* _scene;
        IVideoDriver* _driver;

};

#endif /* !BOMBUP_HPP_ */
