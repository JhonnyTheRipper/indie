/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** Wallpass
*/

#ifndef WALLPASS_HPP_
#define WALLPASS_HPP_

#include <vector>
#include <cstdio>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include "../IObject/IObject.hpp"

class Map;

class WallPass : public IObject{
	public:
		WallPass();
		~WallPass() override {
            if (status) {
                _scene->addToDeletionQueue(node);
                status = !status;
            }
		}
		std::string getName() const override ;
		vector_t getPosition() const override ;
		void setName(std::string name) override;
		bool getStatus() const override ;
        void setTriangleSelector(ITriangleSelector *new_selector) override {};
        IAnimatedMeshSceneNode *getModel() override { return node;};

    void destroy(Map *) override {
		};
		void SetMeshPosition(const vector3df &) override;
		void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override ;
	private:
		std::string name;
		vector_t pos;
		bool status;
		bool traversibility;
        IAnimatedMeshSceneNode *node;
        IAnimatedMesh *_mesh;
		ISceneManager* _scene;
        IVideoDriver* _driver;
};

#endif /* !WALLPASS_HPP_ */
