/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** speed_up
*/

#include "speedUP.hpp"
#include "../Factory/Factory.hpp"

SpeedUp::SpeedUp(): name("SpeedUp"), status(true), traversibility(true)
{	
}

std::string SpeedUp::getName() const
{
    return (this->name);
}

void SpeedUp::setName(std::string name)
{
}

vector_t SpeedUp::getPosition() const
{
    return (this->pos);
}

bool SpeedUp::getStatus() const
{
    return(this->status);
}

void SpeedUp::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const  vector3df &position)
{
    _scene = &scene;
    _driver = &driver;
    _mesh = _scene->getMesh((UP_PATH + "assets/box/model.dae").c_str());
    node = _scene->addAnimatedMeshSceneNode(_mesh, 0, -1, position, vector3df(90, 180, 0), vector3df((float)0.35, (float)0.35, (float)0.35));
    if (this->node) {
        this->node->setMaterialFlag(EMF_LIGHTING, false);
        node->setMaterialTexture(0, _driver->getTexture((UP_PATH + "assets/box/textures/speed_up.png").c_str()));
    }
}

void SpeedUp::SetMeshPosition(const vector3df &pos)
{
    this->node->setPosition(pos);
}