/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** speedUP
*/

#ifndef SPEEDUP_HPP_
#define SPEEDUP_HPP_

#include <vector>
#include <cstdio>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include "../IObject/IObject.hpp"
class Map;

class SpeedUp : public IObject{
	public:
		SpeedUp();
		~SpeedUp() override {
            if (status) {
                _scene->addToDeletionQueue(node);
                status = !status;
            }
		}
		std::string getName() const override ;
		vector_t getPosition() const override ;
		void setName(std::string name) override;
		bool getStatus() const override ;
        IAnimatedMeshSceneNode *getModel() override { return node;};

    void destroy(Map *) override {
		};
		void setTriangleSelector(ITriangleSelector *new_selector) override {};
        void SetMeshPosition(const vector3df &) override;
		void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override ;
	private:
		std::string name;
		vector_t pos;
		bool status;
		bool traversibility;
        IAnimatedMeshSceneNode *node;
        IAnimatedMesh *_mesh;
		ISceneManager* _scene;
		IVideoDriver* _driver;
};

#endif /* !FIREALL_HPP_ */
