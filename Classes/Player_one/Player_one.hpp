/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_one
*/

#ifndef INDIE_PLAYER_ONE_HPP
#define INDIE_PLAYER_ONE_HPP

#include "../Player/Player.hpp"

typedef std::vector<std::vector<std::pair<std::shared_ptr<IObject>, vector_t>>> object_map;

class Player_one : public Player {
public:
    Player_one(MastEventReceiver *, Map *map, std::string prepend = "");
    void bombEvent() override ;
    Direction movementEvent() override;
    void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override;
};


#endif //INDIE_PLAYER_ONE_HPP
