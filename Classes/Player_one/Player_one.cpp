/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_one
*/

#include "Player_one.hpp"
#include "../Map/Map.hpp"

Player_one::Player_one(MastEventReceiver *receiver, Map *map, std::string prepend)
{
    ghosty = false;
    movespeedMultiplier = 0;
    range = 2;
    _prepend = prepend;
    _receiver = receiver;
    name = "First";
    traversibility = true;
    status = true;
    active_bombs = 1;
    _map = map;

    _keyUp = KEY_KEY_Z;
    _keyDown = KEY_KEY_S;
    _keyLeft = KEY_KEY_Q;
    _keyRight = KEY_KEY_D;
    _keyBomb = KEY_SPACE;
}

void Player_one::bombEvent()
{
    handleEvent();
}

Direction Player_one::movementEvent()
{
    return movement();
}

void Player_one::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const  vector3df &position)
{
    _scene = &scene;
    _driver = &driver;
    _position = vector3df(position.X, position.Y, position.Z - PLAYER_Z_OFFSET);
    loadModel(Lich);
}
