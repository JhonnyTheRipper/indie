/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_three
*/

#ifndef INDIE_PLAYER_THREE_HPP
#define INDIE_PLAYER_THREE_HPP

#include "../Player/Player.hpp"

class Player_three : public Player {
public:
    Player_three(MastEventReceiver *, Map *map);
    void bombEvent() override ;
    Direction movementEvent() override ;
    void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override;
};


#endif //INDIE_PLAYER_THREE_HPP
