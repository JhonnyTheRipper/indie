/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_three
*/

#include "Player_three.hpp"

Player_three::Player_three(MastEventReceiver *receiver, Map *map)
{
    ghosty = false;
    movespeedMultiplier = 0;
    range = 2;
    _map = map;
    _receiver = receiver;
    name = "First";
    traversibility = true;
    status = true;
    active_bombs = 1;
    this->iaActualPos.x = 23;
    this->iaActualPos.y = 22;
}

void Player_three::bombEvent()
{

}

Direction Player_three::movementEvent()
{
    return this->iaPlayerThree();
}

void Player_three::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const vector3df &position)
{

    _scene = &scene;
    _driver = &driver;
    _position = position;
    loadModel(Archmage);
}