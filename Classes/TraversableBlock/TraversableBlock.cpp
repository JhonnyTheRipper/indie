/*
** EPITECH PROJECT, 2019
** indie
** File description:
** TraversableBlock
*/

#include "TraversableBlock.hpp"

TraversableBlock::TraversableBlock(): name("Traversable"), status(true), traversibility(true)
{
}

std::string TraversableBlock::getName() const
{
    return(this->name);
}

void TraversableBlock::setName(std::string name)
{
    this->name = name;
}

vector_t TraversableBlock::getPosition() const
{
    return(this->pos);
}

bool TraversableBlock::getStatus() const
{
    return (this->status);
}

void TraversableBlock::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const  vector3df &position)
{
    _mesh = scene.getMesh(nullptr);
    this->node = scene.addAnimatedMeshSceneNode(_mesh, 0, -1, vector3df(0, -4, 15), vector3df(0, 180 , 0));
    if (this->node)
        this->node->setMaterialFlag(EMF_LIGHTING, false);
}

void TraversableBlock::SetMeshPosition(const vector3df &pos)
{
    this->node->setPosition(pos);
}