/*
** EPITECH PROJECT, 2019
** indie
** File description:
** TraversableBlock
*/

#ifndef TRAVERSABLEBLOCK_HPP_
#define TRAVERSABLEBLOCK_HPP_

#include "../IObject/IObject.hpp"
class Map;

class TraversableBlock : public IObject{
	public:
		TraversableBlock();
        std::string getName() const override ;
        void setName(std::string name) override;
        vector_t getPosition() const override ;
        bool getStatus() const override ;
        IAnimatedMeshSceneNode *getModel() override { return node;};

    void destroy(Map *) override {};
        void setTriangleSelector(ITriangleSelector *new_selector) override {_selector = new_selector;};
    	void SetMeshPosition(const vector3df &) override;
        void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override ;
	private:
        std::string name;
        vector_t pos;
        bool status;
        bool traversibility;
        ITriangleSelector *_selector;
        IAnimatedMeshSceneNode *node;
        IAnimatedMesh *_mesh;
        ISceneManager* _scene;
};

#endif /* !TRAVERSABLEBLOCK_HPP_ */
