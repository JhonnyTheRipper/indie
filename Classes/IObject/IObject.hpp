//
// Created by bunny on 16/05/19.
//

#ifndef INDIE_IOBJECT_H
#define INDIE_IOBJECT_H

#include "header.hpp"
class Map;

class IObject {
    public:
        virtual ~IObject() = default;
        virtual std::string getName() const = 0;
        virtual void setName(std::string name) = 0;
        virtual vector_t getPosition() const = 0;
        virtual bool getStatus() const = 0;
        virtual void setTriangleSelector(ITriangleSelector *) = 0;
        virtual void SetMeshPosition(const vector3df &) = 0;
        virtual void destroy(Map *) = 0;
        virtual void CreateIrrlichtObject(ISceneManager &, IVideoDriver &, const vector3df &) = 0;
        virtual IAnimatedMeshSceneNode *getModel() = 0;
};

#endif //INDIE_IOBJECT_H
