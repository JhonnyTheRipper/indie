/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** BOMB
*/

#include "../Classes/Menu/Menu.hpp"
#include <iostream>
#include "header.hpp"
#include "../Classes/Factory/Factory.hpp"
#include "../Classes/EventReicever/EventReicever.hpp"
#include "../Classes/Mast.hpp"
#include "../../Classes/Menu/Menu.hpp"
#include <time.h>
#include <irrlicht/irrlicht.h>

class Game
{
	private:
		MastEventReceiver new_rec;
		IrrlichtDevice *device;
		IVideoDriver *driver;
		ISceneManager *scene;
		IGUIEnvironment *gui;
		IGUIStaticText	*fpsTextElement;
		irr::gui::IGUIEnvironment *env;
		Map *_map;
		irr::video::ITexture *image;
		irr::core::position2d<irr::s32> position0;
		irr::core::position2d<irr::s32> position1;
		irr::core::rect<irr::s32> rectangle;
		irr::video::ITexture *imageM;
		irr::core::position2d<irr::s32> positionM0;
		irr::core::position2d<irr::s32> positionM1;
		irr::core::rect<irr::s32> rectangleM;
		SAppContext context;






	public:
		Game();
		void callGame(SAppContext context);
		void callMenu();
		void loadTexture();
		void endGame();
		void gameLoop();
		void initMap();
		int element;
};

