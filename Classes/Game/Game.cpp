/*
** EPITECH PROJECT, 2019
** OOP_indi_2018
** File description:
** BOMB
*/

#include "Game.hpp"

Game::Game()
{
	this->element = 0;
}

void Game::gameLoop()
{
	int fps = 0;
	bool end;
	while (device->run()) {
		new_rec.endEventProcess();
		fps = driver->getFPS();
		core::stringw str = L"FPS: ";
		str += fps;
		fpsTextElement->setText(str.c_str());
		driver->beginScene(true, true, SColor(255, 100, 100, 140));
		end = _map->playersRunEvents();
		if (end)
            endGame();
		driver->draw2DImage(image, position0, rectangle, 0, irr::video::SColor (255,255,255,255),true);
		scene->drawAll();
		gui->drawAll();
		driver->endScene();
		new_rec.startEventProcess();
	}
}

void Game::callGame(SAppContext context)
{
	this->device = context.device;
	this->env = device->getGUIEnvironment();
	env->clear();
	device->setEventReceiver(&new_rec);
	if (!device)
	    throw("device not created");
	device->setWindowCaption(L"Bomberman Warcraft");
	this->driver = device->getVideoDriver();
	this->scene = device->getSceneManager();
	this->gui = device->getGUIEnvironment();
	this->fpsTextElement = gui->addStaticText(L"", rect<s32>(10, 10, 260, 22), true, false, 0);
    initMap();
}
void Game::endGame()
{
	this->imageM = driver->getTexture((UP_PATH + "image/end.jpg").c_str());
	env->addImage(this->imageM, irr::core::position2d<irr::s32> (0, 0));
	env->drawAll();

}
void Game::initMap()
{
	Map map(&new_rec);
	_map = &map;
    loadTexture();
    map.createMapObject();
	map.render(*scene, *driver);
    map.setCollision(*scene);
    	scene->addCameraSceneNode(0, vector3df(750, 370, -1030), vector3df(750, 600, 0));
	this->image = driver->getTexture((UP_PATH + "image/test.jpg").c_str());
    	this->position0.X = 0;
    	this->position0.Y = 0;
    	this->position1.X = 1920;
    	this->position1.Y = 1080;
    	this->rectangle.UpperLeftCorner = position0;
    	this->rectangle.LowerRightCorner = position1;
    gameLoop();
}
void Game::callMenu()
{
	this->device = createDevice(EDT_OPENGL, core::dimension2d<u32>(1920, 1080));
	srand (unsigned(std::time(0)));
	device->setResizable(true);
	device->setWindowCaption(L"Menu");
	device->setResizable(false);
	video::IVideoDriver* driver = device->getVideoDriver();
	IGUIEnvironment* env = device->getGUIEnvironment();
	this->context.device = device;
	this->imageM = driver->getTexture((UP_PATH + "image/tet2.jpg").c_str());
    	this->positionM0.X = 0;
    	this->positionM0.Y = 0;
    	this->positionM1.X = 1920;
    	this->positionM1.Y = 1080;
    	this->rectangleM.UpperLeftCorner = positionM0;
    	this->rectangleM.LowerRightCorner = positionM1;
	MyEventReceiver receiver(context);
	device->setEventReceiver(&receiver);
	receiver.newMainMenu();
	while (device->run() && driver)
		if (device->isWindowActive()) {
		driver->beginScene(true, true, SColor(0,200,200,200));
		driver->draw2DImage(imageM, positionM0, rectangleM, 0, irr::video::SColor (255,255,255,255), true);
		env->drawAll();
		driver->endScene();
		}
	device->drop();
}

void Game::loadTexture()
{
    IAnimatedMesh *_mesh = scene->getMesh((UP_PATH + "assets/box/model.dae").c_str());
    IAnimatedMeshSceneNode *node = scene->addAnimatedMeshSceneNode(_mesh , 0, -1, vector3df(-200, -200, -200));
    if (node) {
        node->setMaterialTexture(0, driver->getTexture((UP_PATH + "assets/box/textures/bomb_UP.png").c_str()));
        node->setMaterialTexture(0, driver->getTexture((UP_PATH + "assets/box/textures/range.png").c_str()));
        node->setMaterialTexture(0, driver->getTexture((UP_PATH + "assets/box/textures/speed_up.png").c_str()));
        node->setMaterialTexture(0, driver->getTexture((UP_PATH + "assets/box/textures/blockbrick_nrm.png").c_str()));
    }
    scene->addToDeletionQueue(node);
}