/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Menu
*/

/*
** EPITECH PROJECT, 2019
** bootstraps
** File description:
** menu
*/

#ifndef MENU_HPP_
#define MENU_HPP_

#include "header.hpp"
#include <irrlicht/irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

struct SAppContext
{
    IrrlichtDevice *device;
};

enum
{
    GUI_ID_QUIT_BUTTON = 101,
    GUI_ID_START_BUTTON,
    GUI_ID_CONTINUE_BUTTON,
    GUI_ID_INFORMATIONS_BUTTON,
    GUI_ID_1PLAYER_BUTTON,
    GUI_ID_2PLAYER_BUTTON,
    GUI_ID_RETURN_BUTTON,
};

class MyEventReceiver : public IEventReceiver
{
public:
    MyEventReceiver(SAppContext & context) : Context(context) { }
    virtual bool OnEvent(const SEvent& event);
    void newGameMenu();
    void newMainMenu();
    void newInformationsMenu();

private:
    SAppContext & Context;
    irr::video::ITexture *imageM;
	irr::core::position2d<irr::s32> positionM0;
	irr::core::position2d<irr::s32> positionM1;
	irr::core::rect<irr::s32> rectangleM;
};

#endif /* !MENU_HPP_ */