/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Menu
*/

#include "Menu.hpp"
#include "header.hpp"
#include "../Classes/Game/Game.hpp"

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#endif


bool MyEventReceiver::OnEvent(const SEvent& event)
{
    auto *test = new Game();

    if (event.EventType == EET_GUI_EVENT) {
        s32 id = event.GUIEvent.Caller->getID();
        switch(event.GUIEvent.EventType) {
            case EGET_BUTTON_CLICKED:
            switch(id) {
            case GUI_ID_QUIT_BUTTON:
                Context.device->closeDevice();
                return true;
            case GUI_ID_START_BUTTON:
                newGameMenu();
                return true;
            case GUI_ID_INFORMATIONS_BUTTON:
                newInformationsMenu();
                return true;
            case GUI_ID_1PLAYER_BUTTON:
                test->callGame(Context);
                exit(0);
            case GUI_ID_2PLAYER_BUTTON:
                try {
                    test->callGame(Context);
                } catch(std::exception &exe) {
                    std::cerr << exe.what() << std::endl;
                }
                exit(0);
            case GUI_ID_RETURN_BUTTON:
                newMainMenu();
                return true;
            default:
                return false;
            }
        default:
            break;
        }
    }
    return false;
}

void MyEventReceiver::newGameMenu()
{
    IGUIEnvironment* env = Context.device->getGUIEnvironment();
    env->clear();
    env->addButton(rect<s32>(650,320,1160,350 + 32), 0, GUI_ID_1PLAYER_BUTTON,
        L"PLAY", L"Play a Game with 1 player");
    env->addButton(rect<s32>(650,430,1160,460 + 32), 0, GUI_ID_RETURN_BUTTON,
        L"RETURN", L"Return to Main Menu");
}

void MyEventReceiver::newInformationsMenu()
{
    IGUIEnvironment* env = Context.device->getGUIEnvironment();
    env->clear();
    IVideoDriver *driver = Context.device->getVideoDriver();
    this->imageM = driver->getTexture((UP_PATH + "image/information.jpg").c_str());
    env->addImage(this->imageM, irr::core::position2d<irr::s32> (0, 0));
    this->positionM0.X = 0;
    this->positionM0.Y = 0;
    this->positionM1.X = 1920;
    this->positionM1.Y = 1080;
    this->rectangleM.UpperLeftCorner = positionM0;
    this->rectangleM.LowerRightCorner = positionM1;
    driver->beginScene(true, true, SColor(0,200,200,200));
    env->drawAll();
    env->addButton(rect<s32>(150,800,260,830 + 32), 0, GUI_ID_RETURN_BUTTON,
         L"RETURN", L"Return to Main Menu");
}

void MyEventReceiver::newMainMenu()
{
    IGUIEnvironment* env = Context.device->getGUIEnvironment();
    env->clear();
    env->addButton(rect<s32>(660,290,1160,320 + 32), 0, GUI_ID_START_BUTTON,
            L"START", L"Start a new Game");
    env->addButton(rect<s32>(660,400,1160,430 + 32), 0, GUI_ID_CONTINUE_BUTTON,
            L"CONTINUE", L"Loads last Game");
    env->addButton(rect<s32>(660,510,1160,540 + 32), 0, GUI_ID_INFORMATIONS_BUTTON,
            L"INFORMATIONS", L"Opens an information menu");
    env->addButton(rect<s32>(660,620,1160,650 + 32), 0, GUI_ID_QUIT_BUTTON,
            L"QUIT", L"Exits Game");
}

