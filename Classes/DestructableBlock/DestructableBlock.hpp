/*
** EPITECH PROJECT, 2019
** indie
** File description:
** DestructableBlock
*/

#ifndef DESTRUCTABLEBLOCK_H_
#define DESTRUCTABLEBLOCK_H_

#include "../IObject/IObject.hpp"
class Map;

class DestructableBlock : public IObject {
    public :
        DestructableBlock();
        ~DestructableBlock() override {};
        std::string getName() const override;
        void setName(std::string name) override;
        vector_t getPosition() const override;
        bool getStatus() const override;

    void SetMeshPosition(const vector3df &pos);
        void destroy(Map *) override;
        void setTriangleSelector(ITriangleSelector *new_selector) override {_selector = new_selector;};;
        IAnimatedMeshSceneNode *getModel() override { return node;};
        void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override;
    private :
        std::string name;
        vector_t pos;
        bool status;
        bool traversibility;
        IAnimatedMeshSceneNode *node;
        ISceneManager* _scene;
        IVideoDriver* _driver;
        IAnimatedMesh *_mesh;
        ITriangleSelector *_selector;
};

#endif /* !DESTRUCTABLEBLOCK_H_ */