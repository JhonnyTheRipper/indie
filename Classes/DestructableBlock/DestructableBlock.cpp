/*
** EPITECH PROJECT, 2019
** indie
** File description:
** DestructableBlock
*/

#include "DestructableBlock.hpp"
#include "../Map/Map.hpp"


DestructableBlock::DestructableBlock() : name("Destructable"), status(true), traversibility(false)
{
}

std::string DestructableBlock::getName() const
{
    return (this->name);
}

vector_t DestructableBlock::getPosition() const
{
    return (this->pos);
}

bool DestructableBlock::getStatus() const
{
    return(this->status);
}

void DestructableBlock::setName(std::string name)
{
    this->name = name;
}

void DestructableBlock::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const  vector3df &position)
{
    _scene = &scene;
    _driver = &driver;
    irrlicht::FactoryIrr new_fac;
    new_fac.loadMesh("dr_block", *_scene, *_driver, &_mesh, &node, position, vector3df((float)0.60, (float)0.60, (float)0.60));
}

void DestructableBlock::SetMeshPosition(const vector3df &pos)
{
    this->node->setPosition(pos);
}


void DestructableBlock::destroy(Map *map)
{
    if (!status)
        return;
    map->removeSelector(_selector);
    _scene->addToDeletionQueue(node);
    status = !status;
}