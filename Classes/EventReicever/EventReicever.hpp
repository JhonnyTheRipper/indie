/*
** EPITECH PROJECT, 2019
** indie
** File description:
** EventReicever
*/

#ifndef INDIE_EVENTREICEVER_HPP
#define INDIE_EVENTREICEVER_HPP

#include "header.hpp"
#include "irrlicht/irrlicht.h"


class EventReicever : public IEventReceiver {
public:
    EventReicever();
    virtual bool OnEvent(const SEvent &event);
    virtual bool IsKeyDown(EKEY_CODE keyCode) const;

private:
    bool KeyIsDown[KEY_KEY_CODES_COUNT];
};


#endif //INDIE_EVENTREICEVER_HPP
