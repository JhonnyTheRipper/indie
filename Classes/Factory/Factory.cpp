/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Factory
*/

#include "Factory.hpp"
#include <functional>

std::shared_ptr<IObject> base::Factory::createObject(const std::string &name, MastEventReceiver *receiver, Map *map_class)
{
    static const std::unordered_map<std::string, std::function<std::shared_ptr<IObject>()>> map {
            {"playerOne", [&](){ return std::shared_ptr<IObject> (new Player_one(receiver, map_class));}},
            {"playerTwo", [&](){ return std::shared_ptr<IObject> (new Player_two(receiver, map_class));}},
            {"playerThree", [&](){ return std::shared_ptr<IObject> (new Player_three(receiver, map_class));}},
            {"playerFour", [&](){ return std::shared_ptr<IObject> (new Player_four(receiver, map_class));}},
            {"dr_block", [&](){ return std::shared_ptr<IObject> (new DestructableBlock());}},
            {"tr_block", [&](){ return std::shared_ptr<IObject> (new TraversableBlock());}},
            {"ud_block", [&](){ return std::shared_ptr<IObject> (new UndestructableBlock());}},
            {"bomb_up", [&](){ return std::shared_ptr<IObject> (new BombUp());}},
            {"range_up", [&](){ return std::shared_ptr<IObject> (new FireAll());}},
            {"speed_up", [&](){ return std::shared_ptr<IObject> (new SpeedUp());}},
            {"wallpass", [&](){ return std::shared_ptr<IObject> (new WallPass());}}
    };
    const auto end = map.end();
    auto it = map.find(name);
    if (it != end)
        return it->second();
    else {
        throw("The component do not exists");
    }
}

void irrlicht::FactoryIrr::loadMesh(const std::string &name, ISceneManager &scene, IVideoDriver &driver,
                          IAnimatedMesh **mesh, IAnimatedMeshSceneNode **node, const vector3df &position, const vector3df &scale,
                          vector3df rotation, std::string _prepend)
{
    std::string pathToModel;
    std::string pathToTexture;
    static const std::unordered_map<std::string, std::function<void()>> map {
            {"dr_block", [&]() {pathToModel = (_prepend + UP_PATH + "assets/box/model.dae");
                pathToTexture = (_prepend + UP_PATH + "assets/box/textures/blockbrick_alb.png");}},
            {"ud_block", [&]() {pathToModel = (_prepend + UP_PATH + "assets/box/model.dae");
                pathToTexture = (_prepend + UP_PATH + "assets/box/textures/blockbrick_spc.png");}},
    };

    const auto end = map.end();
    auto it = map.find(name);
    if (it != end) {
        it->second();
        (*mesh) = scene.getMesh(pathToModel.c_str());
        (*node) = scene.addAnimatedMeshSceneNode((*mesh), 0, -1, position,
                                                 rotation, scale);

        if (node){
            (*node)->setMaterialFlag(EMF_LIGHTING, false);
            (*node)->setMaterialTexture(0, driver.getTexture(pathToTexture.c_str()));
        }
    }
    else {
        throw("the component do not exists") ;
    }
}