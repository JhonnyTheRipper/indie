/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Factory
*/

#ifndef INDIE_FACTORY_HPP
#define INDIE_FACTORY_HPP

#include <functional>
#include "header.hpp"
#include "../DestructableBlock/DestructableBlock.hpp"
#include "../IObject/IObject.hpp"
#include "../Player/Player.hpp"
#include "../TraversableBlock/TraversableBlock.hpp"
#include "../UndestructableBlock/UndestructableBlock.hpp"
#include "../powerUP/bombUP.hpp"
#include "../powerUP/FireAll.hpp"
#include "../powerUP/speedUP.hpp"
#include "../powerUP/Wallpass.hpp"
#include "../Player_one/Player_one.hpp"
#include "../Player_two/Player_two.hpp"
#include "../Player_three/Player_three.hpp"
#include "../Player_four/Player_four.hpp"
#include "../Map/Map.hpp"
#include "../Mast.hpp"

typedef std::vector<std::vector<std::pair<std::shared_ptr<IObject>, vector_t>>> object_map;

namespace base {
    class Factory {
    public:
        std::shared_ptr<IObject> createObject(const std::string &, MastEventReceiver *receiver, Map *mapobject = nullptr);
    };
}

namespace irrlicht {
    class FactoryIrr {
    public:
        void loadMesh(const std::string &, ISceneManager &, IVideoDriver &,
                      IAnimatedMesh **, IAnimatedMeshSceneNode **, const  vector3df &, const vector3df &, vector3df rotation = vector3df(0, 0, 0), std::string _prepend = "");
    };

}

#endif //INDIE_FACTORY_HPP
