/*
** EPITECH PROJECT, 2019
** indie
** File description:
** UndestructableBlock
*/

#include "UndestructableBlock.hpp"
#include "header.hpp"
#include "../Factory/Factory.hpp"

UndestructableBlock::UndestructableBlock(): name("Undestructable"), status(true), traversibility(false)
{
}

std::string UndestructableBlock::getName() const
{
    return(this->name);
}

void UndestructableBlock::setName(std::string name)
{
    this->name = name;
}

vector_t UndestructableBlock::getPosition() const
{
    return(this->pos);
}

bool UndestructableBlock::getStatus() const
{
    return (this->status);
}

void UndestructableBlock::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const  vector3df &position)
{
    _scene = &scene;
    irrlicht::FactoryIrr new_fac;
    new_fac.loadMesh("ud_block", *_scene, driver, &_mesh, &node, position, vector3df((float)0.60, (float)0.60, (float)0.60));
}

void UndestructableBlock::SetMeshPosition(const vector3df &pos)
{
    this->node->setPosition(pos);
}
