/*
** EPITECH PROJECT, 2019
** indie
** File description:
** undestructableBlock
*/

#ifndef UNDESTRUCTABLEBLOCK_HPP_
#define UNDESTRUCTABLEBLOCK_HPP_

#include "../IObject/IObject.hpp"
class Map;

class UndestructableBlock : public IObject {
    public :
        UndestructableBlock();
        std::string getName() const override ;
        void setName(std::string name) override;
        vector_t getPosition() const override ;
        bool getStatus() const override ;
        IAnimatedMeshSceneNode *getModel() override { return node;};

    void destroy(Map *) override {};
        void setTriangleSelector(ITriangleSelector *new_selector) override {};
	    void SetMeshPosition(const vector3df &) override;
        void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override ;
    private :
        std::string name;
        vector_t pos;
        bool status;
        bool traversibility;
        IAnimatedMeshSceneNode *node;
        IAnimatedMesh *_mesh;
        ISceneManager* _scene;
};

#endif /* !UNDESTRUCTABLEBLOCK_HPP_ */
