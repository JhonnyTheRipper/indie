/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_two
*/

#ifndef INDIE_PLAYER_TWO_HPP
#define INDIE_PLAYER_TWO_HPP

#include "../Player/Player.hpp"

class Player_two : public Player {
public:
    Player_two(MastEventReceiver *, Map *map);
    void bombEvent() override;
    Direction movementEvent() override;
    void CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &, const  vector3df &) override;
};


#endif //INDIE_PLAYER_TWO_HPP
