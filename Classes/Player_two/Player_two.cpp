/*
** EPITECH PROJECT, 2019
** indie
** File description:
** Player_two
*/

#include "Player_two.hpp"

Player_two::Player_two(MastEventReceiver *receiver, Map *map)
{
    ghosty = false;
    movespeedMultiplier = 0;
    range = 2;
    _map = map;
    _receiver = receiver;
    name = "First";
    traversibility = true;
    status = true;
    active_bombs = 1;
}

void Player_two::bombEvent()
{
}

Direction Player_two::movementEvent()
{
    return this->iaPlayerTwo();
}


void Player_two::CreateIrrlichtObject(ISceneManager &scene, IVideoDriver &driver, const vector3df &position)
{
    _scene = &scene;
    _driver = &driver;
    _position = position;
    loadModel(KilJaedan);
}