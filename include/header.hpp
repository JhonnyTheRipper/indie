/*
** EPITECH PROJECT, 2019
** header
** File description:
** header
*/

#ifndef header
#define header


#include <iostream>
#include <utility>
#include <string>
#include <iostream>
#include <cassert>
#include <utility>
#include <type_traits>
#include <unordered_map>
#include <memory>
#include <tuple>
#include <vector>
#include <irrlicht/irrlicht.h>
//#define MAP_SPACING 6.6
#define MAP_SPACING 63

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

enum Move_Directions {
    UP,
    DOWN,
    LEFT,
    RIGHT
};

enum Power_Ups{
    Bomb_Quantity,
    Speed_Boost,
    Bomb_Range,
    Wall_Pass
};

typedef struct Power_Ups_s{
    int bomb_quantity;
    int speed_boost;
    int bomb_range;
    int wall_pass;
}Power_Ups_t;

typedef struct vector_s{
    int x;
    int y;
}vector_t;


enum
{
    // I use this ISceneNode ID to indicate a scene node that is
    // not pickable by getSceneNodeAndCollisionPointFromRay()
            ID_IsNotPickable = 0,

    // I use this flag in ISceneNode IDs to indicate that the
    // scene node can be picked by ray selection.
            IDFlag_IsPickable = 1 << 0,

    // I use this flag in ISceneNode IDs to indicate that the
    // scene node can be highlighted.  In this example, the
    // homonids can be highlighted, but the level mesh can't.
            IDFlag_IsHighlightable = 1 << 1
};

#if defined(__linux) || defined(__linux__) || defined(linux)
#define UP_PATH std::string("../")
#define LINUX

#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32) || defined(_WIN64)
#define UP_PATH std::string("../../")
#define WINDOWS
#endif


#endif /* !header */
