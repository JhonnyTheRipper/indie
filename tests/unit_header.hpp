/*
** EPITECH PROJECT, 2019
** unit_tests
** File description:
** unit_header
*/

#ifndef UNIT_TESTS_UNIT_HEADER_HPP
#define UNIT_TESTS_UNIT_HEADER_HPP

#include <gtest/gtest.h>
#include "header.hpp"
#include "../Classes/Map/Map.hpp"

class Testing_class_Bomb: public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        SIrrlichtCreationParameters params;
        params.DriverType = EDT_NULL;
        params.WindowSize = core::dimension2d<u32>(1, 1);
        params.LoggingLevel = ELL_NONE;
        device = createDeviceEx(params);
        _driver = device->getVideoDriver();
        _scene  = device->getSceneManager();
        _map = new Map(nullptr, "../");
        _map->createMapObject();
    }
    virtual void TearDown()
    {
        device->drop();
    }

    Map *_map;
    IrrlichtDevice *device;
    IVideoDriver *_driver;
    ISceneManager *_scene;
};

class MapTests: public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        SIrrlichtCreationParameters params;
        params.DriverType = EDT_NULL;
        params.WindowSize = core::dimension2d<u32>(1, 1);
        params.LoggingLevel = ELL_NONE;
        device = createDeviceEx(params);
        _driver = device->getVideoDriver();
        _scene  = device->getSceneManager();
    }
    virtual void TearDown()
    {
        device->drop();
    }
    IrrlichtDevice *device;
    IVideoDriver *_driver;
    ISceneManager *_scene;
};


class FactoryTests: public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        SIrrlichtCreationParameters params;
        params.DriverType = EDT_NULL;
        params.WindowSize = core::dimension2d<u32>(1, 1);
        params.LoggingLevel = ELL_NONE;
        device = createDeviceEx(params);
        _driver = device->getVideoDriver();
        _scene  = device->getSceneManager();
    }
    virtual void TearDown()
    {
        device->drop();
    }
    IrrlichtDevice *device;
    IVideoDriver *_driver;
    IAnimatedMesh *_mesh;
    IAnimatedMeshSceneNode *_node;
    ISceneManager *_scene;
};
#endif //UNIT_TESTS_UNIT_HEADER_HPP
