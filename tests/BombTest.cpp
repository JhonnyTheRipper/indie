/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** BombTest
*/
#include "unit_header.hpp"
#include "../Classes/Bomb/Bomb.hpp"

TEST_F(Testing_class_Bomb, Bomb_get_position_x)
{
    Bomb _bomb(*_scene, *_driver, vector3df(126, 126, 126), nullptr, 2, "../");
    auto pos = _bomb.getPosition();
    vector_t to_comp{2, 2};
    ASSERT_EQ(pos.x, to_comp.x);
    ASSERT_NE(pos.y, 0);
}

TEST_F(Testing_class_Bomb, Bomb_get_position_t)
{
    Bomb _bomb(*_scene, *_driver, vector3df(126, 126, 126), nullptr, 2, "../");
    auto pos = _bomb.getPosition();
    vector_t to_comp{2, 2};
    ASSERT_EQ(pos.y, to_comp.y);
    ASSERT_NE(pos.y, 0);
}

TEST_F(Testing_class_Bomb, Bomb_getName)
{
    Bomb _bomb(*_scene, *_driver, vector3df(126, 126, 126), nullptr, 2, "../");
    ASSERT_EQ("Bomb", _bomb.getName());
    ASSERT_STREQ("Bomb", _bomb.getName().c_str());
    ASSERT_EQ(4, _bomb.getName().length());
}

TEST_F(Testing_class_Bomb, Bomb_getStatus)
{
    Bomb _bomb(*_scene, *_driver, vector3df(126, 126, 126), nullptr, 2, "../");
    bool s = _bomb.getStatus();
    ASSERT_NE(false, s);
    ASSERT_EQ(true, s);
}

TEST_F(Testing_class_Bomb, Bomb_setName)
{
    Bomb _bomb(*_scene, *_driver, vector3df(126, 126, 126), nullptr, 2, "../");
    _bomb.setName("another bomb");
    auto new_name = _bomb.getName();
    ASSERT_EQ("another bomb", new_name);
    ASSERT_STREQ("another bomb", new_name.c_str());
    ASSERT_EQ(12, new_name.length());
}