/*
** EPITECH PROJECT, 2019
** unit_tests
** File description:
** MapTest
*/

#include "unit_header.hpp"
#include "../Classes/Factory/Factory.hpp"

TEST_F(FactoryTests, ObjectCreation)
{
    base::Factory base_factory;
    std::shared_ptr<IObject> object;
    EXPECT_NO_THROW(object = base_factory.createObject("ud_block", nullptr));
    ASSERT_NE(object, nullptr);
    EXPECT_ANY_THROW(object = base_factory.createObject("luoiuoii", nullptr));
}

TEST_F(FactoryTests, IrrlichObjectCreation)
{
    irrlicht::FactoryIrr new_factory;
    EXPECT_NO_THROW(new_factory.loadMesh("dr_block", *_scene, *_driver, &_mesh, &_node,
            vector3df(20, 20, 20), vector3df(20, 20, 20), vector3df(0, 0, 0), "../"));
    ASSERT_NE(nullptr, _mesh);
    ASSERT_NE(nullptr, _node);
    EXPECT_ANY_THROW(new_factory.loadMesh("FALSE_BLOCK", *_scene, *_driver, &_mesh, &_node,
                                         vector3df(20, 20, 20), vector3df(20, 20, 20), vector3df(0, 0, 0), "../"));
}