/*
** EPITECH PROJECT, 2019
** unit_tests
** File description:
** MapTest
*/

#include "unit_header.hpp"
#include "../Classes/Map/Map.hpp"

TEST_F(MapTests, VectorOfPlayersGetter)
{
    Map _map(nullptr, "../");
    auto players = _map.getPlayersVector();
    ASSERT_EQ(0, players.size());
}

TEST_F(MapTests, MapCreation)
{
    Map _map(nullptr, "../");
    auto map_object = _map.getMapObject();
    ASSERT_EQ(0, map_object.size());
    _map.createMapObject();
    map_object = _map.getMapObject();
    ASSERT_NE(0, map_object.size());
    ASSERT_NE(0, map_object[0].size());
    ASSERT_NE(nullptr, map_object[0][0].first);
}

TEST_F(MapTests, PlayersGetting)
{
    Map _map(nullptr, "../");
    auto player_one = _map.get_player_one();
    auto player_two = _map.get_player_two();
    auto player_three = _map.get_player_three();
    auto player_four = _map.get_player_four();
    ASSERT_EQ(nullptr, player_one);
    ASSERT_EQ(nullptr, player_two);
    ASSERT_EQ(nullptr, player_three);
    ASSERT_EQ(nullptr, player_four);
    _map.createMapObject();

    player_one = _map.get_player_one();
    player_two = _map.get_player_two();
    player_three = _map.get_player_three();
    player_four = _map.get_player_four();
    ASSERT_NE(nullptr, player_one);
    ASSERT_NE(nullptr, player_two);
    ASSERT_NE(nullptr, player_three);
    ASSERT_NE(nullptr, player_four);
}